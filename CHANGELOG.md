## 1.1.0

  * Removed dependencies of Flutter SDK

  * Renamed `RefExtension on Object` to `RefObjectExtension on Object`

  * Added `RefExtension<T> on Ref<T>`

  * Increased Dart SDK constraints to `3.0.0`

## 1.0.2

  * **[ref.dart](lib/src/ref.dart)**:
    
    * Changed calculation of `hashCode`, because if the internal _ref held
      an `Iterable` as it's value, then calling `hashList`, on the _ref caused an `AssertationError`. 
      
      * **OLD**
        ```dart
          int get hashCode => hashValues(_ref.hashCode, hashList(_ref), value.hashCode);
        ```

      * **NEW**
        ```dart
          int get hashCode => hashValues(_ref.hashCode, value.hashCode);
        ``` 

---

  * **[object_reference_test.dart](test/object_reference_test.dart)**:
    
    * Added `test("Iterables", () { ... });` for testing the new implementation of `Ref.hashCode`.

## 1.0.1

  * **[ref.dart](lib/src/ref.dart)**:
    
    * Added factory constructors `Ref.mutable` and `Ref.constant`.

## 1.0.0

  * Added **[const_ref.dart](lib/src/const_ref.dart)**.

  * Added **[extension.dart](lib/src/extension.dart)**.
 
  * Added **[mutable_ref.dart](lib/src/mutable_ref.dart)**.

  * Added **[ref.dart](lib/src/ref.dart)**.

  For detailed information of the added files, read their dartdoc.

  * Added tests in **[object_reference_test.dart](test/object_reference_test.dart)**.

  * Implemented **[README.md](README.md)**.

---

## 0.0.1

* Initial release.
