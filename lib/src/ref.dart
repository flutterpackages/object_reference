part of "../object_reference.dart";

/// A reference to an [Object] of type [T].
///
/// ## How does the equality work?
///
/// ```dart
/// void main() {
///   final r1 = "nona".ref();
///   final r2 = "nona".ref();
///   final rc1 = r1.constRef();
///
///   // false, because r1 and r2 hold an Object with the same
///   // String content, but r1 and r2 are not references to
///   // the same String Object.
///   print(r1 == r2);
///
///   // false, because r1 and r2 hold an Object with the same
///   // String content, but r1 and r2 are not references to
///   // the same String Object.
///   print(r1.hashCode == r2.hashCode);
///
///   // true, because r1 and r2 hold an Object with the same
///   // String content. Here it does not matter that r1 and r2
///   // are not references to the same String Object, because
///   // we are only checking if the Strings have the same content.
///   print(r1.value == r2.value);
///
///   // true, because r1 and rc1 are a reference to the same
///   // String Object. It does not matter that r1 is a MutableRef
///   // and rc1 is a ConstRef, because only the underlying value
///   // of references is compared.
///   print(r1 == rc1);
///
///   // true, because r1 and rc1 are a reference to the same
///   // String Object. It does not matter that r1 is a MutableRef
///   // and rc1 is a ConstRef, because only the underlying value
///   // of references is used for calculating the hashCode.
///   print(r1.hashCode == rc1.hashCode);
/// }
/// ```
sealed class Ref<T> {
  Ref(T value)
      : assert(value is! Ref, "Do not create References of References."),
        _ref = [value];

  /// Constructor for copying the underlying [ref] of other [Ref] Objects
  /// and assigning it to `this` [Ref] as well.
  Ref._internal(List<T> ref)
      : assert(ref.first is! Ref, "Do not create References of References."),
        _ref = ref;

  factory Ref.mutable(T value) => MutableRef._(value);

  factory Ref.constant(T value) => ConstRef._(value);

  // Because lists in dart are always passed as pointers to their head,
  // we can abuse this feature by storing our "reference" object
  // as the first and only element of our list.
  late final List<T> _ref;

  T get value => _ref.first;

  set value(T value) => _ref.first = value;

  @override
  bool operator ==(Object other) {
    // When comparing references we are only interested in the underlying
    // _ref, and the value that the _ref List holds.

    if (other is! Ref<T>) return false;
    if (_ref != other._ref) return false;
    if (value != other.value) return false;

    return true;
  }

  @override
  int get hashCode => Object.hash(_ref.hashCode, value.hashCode);

  @override
  String toString() => "${describeIdentity(this)}($value)";
}

/// A [Ref] to an [Object] of type [T], of which you
/// can modify the value.
class MutableRef<T> extends Ref<T> {
  MutableRef._(T value) : super(value);

  MutableRef._fromRef(Ref<T> ref) : super._internal(ref._ref);
}

/// A [Ref] to an [Object] of type [T], of which you
/// cannot modify the value.
class ConstRef<T> extends Ref<T> {
  ConstRef._(T value) : super(value);

  ConstRef._fromRef(Ref<T> ref) : super._internal(ref._ref);

  @override
  set value(T value) => throw StateError(
        "Setting the value of "
        "a ConstRef is not allowed!",
      );
}
