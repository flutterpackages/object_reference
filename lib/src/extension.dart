part of "../object_reference.dart";

extension RefObjectExtension on Object {
  /// Get a [MutableRef] of the [Object] this method was called on.
  MutableRef<T> ref<T>() => (this is Ref<T>) ? ref() : MutableRef._(this as T);

  /// Get a [ConstRef] of the [Object] this method was called on.
  ConstRef<T> constRef<T>() =>
      (this is Ref<T>) ? constRef() : ConstRef._(this as T);
}

extension RefExtension<T> on Ref<T> {
  /// Converts this [Ref] to a [MutableRef].
  // ignore: use_to_and_as_if_applicable
  MutableRef<T> ref() => MutableRef<T>._fromRef(this);

  /// Converts this [Ref] to a [ConstRef].
  // ignore: use_to_and_as_if_applicable
  ConstRef<T> constRef() => ConstRef<T>._fromRef(this);
}
