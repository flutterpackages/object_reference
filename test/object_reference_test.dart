// ignore_for_file: unrelated_type_equality_checks

import "package:collection/collection.dart";
import "package:object_reference/object_reference.dart";
import "package:test/test.dart";

void main() {
  final listEquals = const DeepCollectionEquality().equals;

  group("Check equality: ", () {
    test("Non collection objects", () {
      final r1 = "nona".ref();
      final r2 = "nona".ref();
      final rc1 = r1.constRef();

      expect(r1, isA<MutableRef<dynamic>>());
      expect(r2, isA<MutableRef<dynamic>>());
      expect(rc1, isA<ConstRef<dynamic>>());

      // false, because r1 and r2 hold an Object with the same
      // String content, but r1 and r2 are not references to
      // the same String Object.
      expect(r1 == r2, false);

      // false, because r1 and r2 hold an Object with the same
      // String content, but r1 and r2 are not references to
      // the same String Object.
      expect(r1.hashCode == r2.hashCode, false);

      // true, because r1 and r2 hold an Object with the same
      // String content. Here it does not matter that r1 and r2
      // are not references to the same String Object, because
      // we are only checking if the Strings have the same content.
      expect(r1.value == r2.value, true);

      // true, because r1 and rc1 are a reference to the same
      // String Object. It does not matter that r1 is a MutableRef
      // and rc1 is a ConstRef, because only the underlying value
      // of references is compared.
      expect(r1 == rc1, true);

      // true, because r1 and rc1 are a reference to the same
      // String Object. It does not matter that r1 is a MutableRef
      // and rc1 is a ConstRef, because only the underlying value
      // of references is used for calculating the hashCode.
      expect(r1.hashCode == rc1.hashCode, true);
    });

    test("Iterables", () {
      final r1 = ["nona"].ref<List<String>>();
      final r2 = ["nona"].ref<List<String>>();
      final rc1 = r1.constRef();

      expect(r1, isA<MutableRef<List<String>>>());
      expect(r2, isA<MutableRef<List<String>>>());
      expect(rc1, isA<ConstRef<List<String>>>());

      // false, because r1 and r2 hold an Object with the same
      // String content, but r1 and r2 are not references to
      // the same String Object.
      expect(r1 == r2, false);

      // false, because r1 and r2 hold an Object with the same
      // String content, but r1 and r2 are not references to
      // the same String Object.
      expect(r1.hashCode == r2.hashCode, false);

      // false, because r1 and r2 hold two seperatly
      // instantiated Lists. Therefore the value of
      // r1 and r2 CANNOT be the same.
      expect(r1.value == r2.value, false);

      // true, because r1 and r2 hold two seperatly
      // instantiated Lists, but those Lists contain the
      // same elements, in the same iteration order.
      expect(listEquals(r1.value, r2.value), true);

      // true, because r1 and rc1 are a reference to the same
      // String Object. It does not matter that r1 is a MutableRef
      // and rc1 is a ConstRef, because only the underlying value
      // of references is compared.
      expect(r1 == rc1, true);

      // true, because r1 and rc1 are a reference to the same
      // String Object. It does not matter that r1 is a MutableRef
      // and rc1 is a ConstRef, because only the underlying value
      // of references is used for calculating the hashCode.
      expect(r1.hashCode == rc1.hashCode, true);
    });
  });

  test("Check reference parameters for functions.", () {
    void modifyInt(MutableRef<int> i) {
      i.value = 10;
    }

    final ref = 0.ref<int>();

    // 0, because we initialized it with this value.
    expect(ref.value, 0);

    modifyInt(ref);

    //  10, because modifyInt sets the value of ref to 10.
    expect(ref.value, 10);
  });
}
