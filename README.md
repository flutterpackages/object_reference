A simple object reference system in dart.

## Features

Normally it is impossible in languages like dart to pass an Object like an int to a method and change the value of the passed parameter, because all Objects, except for collections, are passed by value.

With this package you can create a method that accepts a reference and modify the value of the reference in the method.

```dart
import 'package:object_reference/object_reference.dart';

void main() {
  final ref = 0.ref<int>();

  // Prints 0, because we initialized it with this value.
  print(ref.value);

  modifyInt(ref);

  // Prints 10, because modifyInt sets the value of ref to 10.
  print(ref.value);
}

void modifyInt(MutableRef<int> i) {
  i.value = 10;
}

```

## Getting started

Just import `import 'package:object_reference/object_reference.dart';` in the dart file in which you want to use references and you are good to go.

## Usage

You can use the constructors of the various reference types to create a reference of a specific value, or you can just call the `.ref()`, or `.constRef()` method on any Object.